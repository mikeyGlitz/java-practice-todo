package com.test.tasks;

import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.DefaultValidationResultModel;
import com.jgoodies.validation.view.ValidationResultViewFactory;
import com.jidesoft.swing.LabeledTextField;
import com.test.tasks.bean.Task;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    private JPanel rootPanel;
    private JList taskList;
    private JPanel taskEntryPanel;
    private LabeledTextField taskTextField;
    private JButton submitButton;
    private JScrollPane validationResultScrollPane;
    private JPanel validationResultPane;
    private JComponent validationResultsComponent;
    private ArrayList<Task> todos = new ArrayList<>();
    private ValidationResultModel resultModel = new DefaultValidationResultModel();

    public App() {
        submitButton.addActionListener((ActionEvent e) -> {
            this.resultModel.setResult(this.validate());
            if (!this.resultModel.hasErrors()) {
                Task task = new Task(this.taskTextField.getText());
                todos.add(task);

                ((DefaultListModel<String>) taskList.getModel()).add(0, task.getDescription());
            }
            this.rootPanel.repaint();
            this.rootPanel.revalidate();
        });

        JComponent validationList = ValidationResultViewFactory.createReportList(this.resultModel);
        this.validationResultPane.add(validationList, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(() -> {
            // Set the look and feel
            try {
                UIManager.setLookAndFeel(com.jgoodies.looks.windows.WindowsLookAndFeel.class.getName());
            } catch (
                    InstantiationException
                            | IllegalAccessException
                            | UnsupportedLookAndFeelException
                            | ClassNotFoundException e
            ) {
                Logger.getLogger(App.class.getName()).log(Level.INFO, "Unable to load Look and Feel");
            }
            JFrame mainWindow = new JFrame("Tasks App");
            mainWindow.setContentPane(new App().rootPanel);
            mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            mainWindow.pack();
            mainWindow.setVisible(true);
        });
    }

    private ValidationResult validate() {
        ValidationResult result = new ValidationResult();
        if (StringUtils.isEmpty(this.taskTextField.getText())) {
            result.addError("Task text cannot be empty");
        }

        return result;
    }

    private void createUIComponents() {
        taskList = new JList();
        taskList.setModel(new DefaultListModel<String>());
    }
}
