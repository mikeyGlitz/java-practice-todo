package com.test.tasks.bean

import com.jgoodies.common.bean.Bean

class Task(val description: String) : Bean() {
    companion object {
        val PROPERTY_DESCRIPTION = "description";
    }
}
