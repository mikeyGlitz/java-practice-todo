# Java Todo List
This is a sample to do list application to gain familiarity with
Java and Swing. This application was built with [Gradle](gradle.org)
and requires Gradle to be installed before running.

Since this is a simple application there aren't may dependencies.
The application was built using JDK 8 or higher and the JIDE-OSS
and JGoodies libraries.

This project also utilizes Kotlin to simplify the bean and validator
syntax.

## License
This project uses the GPL-3 License

## Contributing
Since this is a throw-away application, contributing isn't really
expected.